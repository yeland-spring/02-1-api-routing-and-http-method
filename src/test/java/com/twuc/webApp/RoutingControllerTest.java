package com.twuc.webApp;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoutingControllerTest {

    private RoutingController bookController = new RoutingController();

    @Test
    void should_return_book_when_request() {
        String book = bookController.getBook(2);
        assertEquals("The book for user 2", book);
    }

    @Test
    void should_return_book_when_variables_is_different() {
        String book = bookController.getAnotherBook("Sam");
        assertEquals("Book for Sam", book);
    }
}
