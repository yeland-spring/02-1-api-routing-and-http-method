package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class RoutingControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_status_200_with_book_when_request() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/2/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 2"));
    }

    @Test
    void should_return_status_200_with_book_when_variables_different () throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/authors/Sam/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Book for Sam"));
    }

    @Test
    void should_return_segment_when_request() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/good"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("good"));
    }

    @Test
    void should_return_status_when_use_question_mark_given_two() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/chapter31-content"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_return_status_when_use_question_mark_given_none() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/chapter-content"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_return_message_when_use_star_mark() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcards/star"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("This is star"));
    }

    @Test
    void should_return_message_when_use_star_mark_in_the_middle() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/star/after"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("This is middle star"));
    }

    @Test
    void should_return_status_when_use_star_mark_given_none() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcard/after"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_return_message_when_use_star_mark_get_prefix_and_suffix() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/presenter"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("This is star"));
    }

    @Test
    void should_return_message_when_use_double_star() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/books/title/page/content"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("This is double star"));
    }

    @Test
    void should_return_message_when_use_regex() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/methods/Get"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("Match successfully"));
    }

    @Test
    void should_return_message_when_request_with_parameter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/test?name=request"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("successful"));
    }

    @Test
    void should_return_message_given_no_parameter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/test"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_400_when_query_string_is_disabled() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/colors?id=2"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }
}