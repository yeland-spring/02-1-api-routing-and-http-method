package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

@RestController
public class RoutingController {

    @PostMapping("/api/users/{userId}/books")
    public String getBook(@PathVariable int userId) {
        return String.format("The book for user %d", userId);
    }

    @PostMapping("/api/authors/{Name}/books")
    public String getAnotherBook(@PathVariable("Name") String name) {
        return String.format("Book for %s", name);
    }

    @GetMapping("/api/segments/good")
    public String getGood() {
        return "good";
    }

    @GetMapping("/api/segments/{segment}")
    public String getSegment(@PathVariable String segment) {
        return "segment" + segment;
    }

    @GetMapping("/api/chapter?-content")
    public String matchQuestionMore() {
        return "Match successfully";
    }

    @GetMapping("/api/wildcards/*")
    public String matchStar() {
        return "This is star";
    }

    @GetMapping("/api/wildcard/*/after")
    public String matchMiddleStar() {
        return "This is middle star";
    }

    @GetMapping("/api/pre*ter")
    public String matchPrefixAndSuffix() {
        return "This is star";
    }

    @GetMapping("/api/books/**/content")
    public String matchDoubleStar() {
        return "This is double star";
    }

    @GetMapping("/api/methods/{name:[A-Za-z]*}")
    public String matchRegex(@PathVariable String name) {
        return "Match successfully";
    }

    @GetMapping("/api/test")
    public String useRequest(@RequestParam String name) {
        return "successful";
    }

    @GetMapping(value = "/api/colors", params = "!id")
    public String colorRequest(@RequestParam String name) {
        return "successful";
    }
}
